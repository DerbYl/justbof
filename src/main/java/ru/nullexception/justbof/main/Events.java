package ru.nullexception.justbof.main;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.Plugin;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import ru.nullexception.justbof.lib.ServerActions;
import ru.nullexception.justbof.popupchat.Buffer;
import ru.nullexception.justbof.popupchat.Chat;
import ru.nullexception.justbof.utils.Postgres;
import ru.nullexception.justbof.utils.Sqlite;

public class Events implements Listener {
	
	Plugin plugin;
	int localMessageDistance;
	private Buffer buffer;
	public Chat bubbles;
	public boolean runTask1 = false;
	
	public Events(Plugin plugin, int localMessageDistance, Buffer buffer, Chat bubbles) {
		this.localMessageDistance = localMessageDistance;
		this.plugin = plugin;
		this.bubbles = bubbles;
		this.buffer = buffer;
	}

	@EventHandler
	void RMBaction(PlayerInteractEntityEvent event) {
		Player player = event.getPlayer();
		if(event.getRightClicked() != null && event.getPlayer().getInventory().getItemInMainHand() != null && event.getRightClicked() instanceof org.bukkit.entity.Rabbit) {
			event.setCancelled(true);
			event.getPlayer().sendMessage(ChatColor.RED + "Кроликов нельзя размножить");
		} else if(event.getRightClicked() instanceof Player) {
			player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new ComponentBuilder(net.md_5.bungee.api.ChatColor.AQUA + "Это " + event.getRightClicked().getName() + net.md_5.bungee.api.ChatColor.RESET).create());
		}
	}

	@EventHandler
	public void adminMove(PlayerMoveEvent e) {
		Player player = e.getPlayer();
		if(player.getUniqueId().toString().equals("ef449501-5601-4f45-a26b-7cb20e08d8e8")) {
			Core.kust_x++;
			if(Core.kust_x == 5) {
				Core.kust_x = 0;
				Location l = player.getLocation();
				l.setY(l.getY() + 2);
				player.spawnParticle(Particle.ENCHANTMENT_TABLE, l, 2);
			}
		}
	}

	// @EventHandler
	// public void onPlayerQuit(PlayerQuitEvent e) {
	// 	Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	// 	Sqlite.exec(Core.plugin.getConfig().getString("SQLite_path2"), "INSERT INTO graph1 (time, tps, players) VALUES (" + Long.toString(timestamp.getTime()) + "," + (double)(Math.round(Arrays.stream(MinecraftServer.getServer().recentTps).max().getAsDouble() * 100.0) / 100L) + "," + Bukkit.getServer().getOnlinePlayers().size() + ")");
	// }

	@EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
        player.sendMessage(ServerActions.welcomeMessage(player));
		player.sendMessage(ChatColor.GOLD + Core.plugin.getDescription().getName() + " " + Core.plugin.getDescription().getVersion());

		if(!Core.getDataBaseGameData().UUIDisExists(String.join("", player.getUniqueId().toString().split("-")))) {
			Core.getDataBaseGameData().InsertUUID(String.join("", player.getUniqueId().toString().split("-")));
		}

		// Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		// Sqlite.exec(Core.plugin.getConfig().getString("SQLite_path2"), "INSERT INTO graph1 (time, tps, players) VALUES (" + Long.toString(timestamp.getTime()) + "," + (double)(Math.round(Arrays.stream(MinecraftServer.getServer().recentTps).max().getAsDouble() * 100.0) / 100L) + "," + Bukkit.getServer().getOnlinePlayers().size() + ")");
		
		if(Core.plugin.getDescription().getVersion().split("-")[1].equals("snapshot")) {
			player.sendMessage(ChatColor.RED + "При возникновений проблем с плагином пишите сюда: " + Core.plugin.getConfig().getString("IssueUrl"));
		}
	}

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		event.setCancelled(true);
		if(Character.toString(event.getMessage().charAt(0)).equals("!")) {
			String currentMessage = "";
			if(event.getMessage().length() > 1) {
				currentMessage = event.getMessage().substring(1, event.getMessage().length());
			} else { currentMessage = "!"; }
			
			TextComponent confirmGlobalMessage = new TextComponent();
			confirmGlobalMessage.addExtra(
				"[" + ChatColor.AQUA + "BoF" + ChatColor.WHITE + "] " + event.getPlayer().getName() + ": " + currentMessage
			);
			
			confirmGlobalMessage.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(
				"Скопировать сообщение игрока " + player.getName()
			).create()));
			
			confirmGlobalMessage.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND,
				"!(" + player.getName() + ": " + currentMessage + ") ")
			);
			
			Bukkit.spigot().broadcast(confirmGlobalMessage);
//			for(Player p : Bukkit.getOnlinePlayers()) {
//				for(String uuid : Core.getBL()) {
//					// Доделать
//				}
//			}
			Bukkit.getLogger().info("g, " + player.getName() + ", " + currentMessage + " [" +
			Double.toString(player.getLocation().getX()) + ";" +
			Double.toString(player.getLocation().getY()) + ";" +
			Double.toString(player.getLocation().getZ()) +
			"]");
		} else if (Character.toString(event.getMessage().charAt(0)).equals("%")) {

            if (event.getMessage().length() > 1) {
                currentMessage = event.getMessage().substring(1, event.getMessage().length());
            } else {
                currentMessage = "%";
            }

			String sendMessage = ChatColor.GRAY + "(( " + event.getPlayer().getName() + ": " + currentMessage + " ))";
            MessageMailing.localMessage(player, sendMessage, 50);

        } else {
			event.setCancelled(true);
			buffer.receiveChat(event.getPlayer(), event.getMessage());
			ServerActions.localMessage(event.getPlayer(), event.getMessage(), localMessageDistance);
			Bukkit.getLogger().info(
				"l, " + player.getName() + ", " + event.getMessage() + " [" +
				Double.toString(player.getLocation().getX()) + ";" +
				Double.toString(player.getLocation().getY()) + ";" +
				Double.toString(player.getLocation().getZ()) +
				"]"
			);
		}
	}

	@EventHandler
	public void execCmd(PlayerCommandPreprocessEvent event) {
		Player player = event.getPlayer();
		String cmd = event.getMessage().split(" ")[0];
		List<?> blockedCmds = Core.plugin.getConfig().getList("blocked_cmds");
		for(int i = 0; i < blockedCmds.size(); i++) {
			if(cmd.equals("/" + blockedCmds.get(i)) || cmd.equals("/minecraft:" + blockedCmds.get(i))) {
				event.setCancelled(true);
				event.getPlayer().sendMessage(ChatColor.RED + "Команда заблокирована. Используйте /t" + ChatColor.RESET);
				return;
			}
		}
		if(cmd.equals("/co") || cmd.equals("/coreprotect:co")) {
			if(!player.isOp()) {
				if(Core.getDataBaseGameData().CompareAL(String.join("", player.getUniqueId().toString().split("-")), 1)) {
					event.setCancelled(false);
				} else {
					event.setCancelled(true);
					player.sendMessage(ChatColor.RED + "Доступ запрещён");
				}
//					default:
//						event.setCancelled(true);
//						player.sendMessage(ChatColor.AQUA + "В настоящее время доступ к CP отключён по причине: " + Core.plugin.getConfig().getString("reason1"));
//						break;
			} else {
				event.setCancelled(false);
			}
		}
		else if(cmd.equals("/lay") || cmd.equals("/simplesit:lay")) {
			player.sendMessage(ChatColor.RED + "Не работает" + ChatColor.RESET);
			event.setCancelled(true);
		} else if(cmd.equals("/minecraft:me")) {
			player.sendMessage(ChatColor.RED + "Не работает" + ChatColor.RESET);
			event.setCancelled(true);
		} else if(cmd.equals("/restart") || cmd.equals("/minecraft:restart")){
			event.getPlayer().sendMessage(ChatColor.RED + "Хома, если это ты, то иди на хер со своим рестартом, после этого надо сервак стопить и выводить обратно на скрин. Только stop и start" + ChatColor.RESET);
			event.setCancelled(true);
		} else {
			event.setCancelled(false);
		}
	}
}
