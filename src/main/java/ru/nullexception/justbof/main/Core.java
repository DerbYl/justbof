package ru.nullexception.justbof.main;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;
import ru.derby.justbof.cmds.Dice;
import ru.nullexception.justbof.cmds.*;
import ru.nullexception.justbof.cmds.Modules;
import ru.nullexception.justbof.eggs.Pizza;
import ru.nullexception.justbof.eggs.Pokemon;
import ru.nullexception.justbof.experimental.CheckChests;
import ru.nullexception.justbof.experimental.Commands;
import ru.nullexception.justbof.popupchat.Buffer;
import ru.nullexception.justbof.popupchat.Chat;
import ru.nullexception.justbof.utils.DataBaseSQLite;
import ru.nullexception.justbof.utils.Postgres;

//import universe.planets.earth.country.russia.Нологи;

public class Core extends JavaPlugin {
	public static Plugin plugin;
	private static List<String> _bl = new ArrayList<String>();
	public Chat bubbles;
	Buffer buffer;
	public ArrayList<Integer> BackgroundTasks = new ArrayList<Integer>();
	Random rand = new Random();
	private static DataBaseSQLite _bd_game_data = new DataBaseSQLite("game_data.db");

	public static DataBaseSQLite getDataBaseGameData() {
		return _bd_game_data;
	}
	// EXPERIMENTAL //////////////////////////////////////////////////////////////////////
	public static List<String> t = null;
	Material[] m = null;
	public static int kust_x = 0;
	// private static HashMap<String, GameTrigger> _gts = null;
	// private static PlayerController _gpc = new PlayerController();
	//////////////////////////////////////////////////////////////////////////////////////
	
	// public static PlayerController getPC() {
	// 	return _gpc;
	// }

	// public static GameTrigger getGameTrigger(String name) {
	// 	return _gts.get(name);
	// }

	public static List<String> getBL() {
		return _bl;
	}

	public void onEnable() {
		// if(getConfig().getBoolean("experimentalFunctions")) {
		// 	_gts = new HashMap<String, GameTrigger>();
		// 	_gts.put("AFKMarket", new GameTrigger(new Location(getServer().getWorlds().get(0), 148, 8, 28), new Location(getServer().getWorlds().get(0), 156, 4, 37)));
		// 	// CreateLogs(new String[]{"cc", "afk_market"}); // NOT WORKING
		// 	t = new ArrayList<String>();
		// 	this.m = Material.values();
		// }

		// for(Player p : Bukkit.getOnlinePlayers()) {
		// 	p.sendMessage(org.bukkit.ChatColor.AQUA + "Плагин " + Core.plugin.getDescription().getName() + " обновлен");
		// }

		this.bubbles = new Chat(this);
		this.buffer = new Buffer(this);
		plugin = this;
		getLogger().info("Plugin enabled");
		
		RegisterCommands();
		RegisterEvents();
		DefaultConfigs();
		RegisterBackgroundTasks();
	}

	public void onDisable() {
		getLogger().info("Plugin disabled");
		this.BackgroundTasks.forEach(ID -> Bukkit.getScheduler().cancelTask((int) ID));
	}
	
	private void DefaultConfigs() {
		File config = new File(getDataFolder() + File.separator + "config.yml");
		if (!config.exists()) {
			getConfig().options().copyDefaults(true);
			saveDefaultConfig();
			getLogger().info("Config 'config' has been created");
		} else { getLogger().info("Config 'config' already exists"); }
	}

	// private void CreateLogs(String[] names) {
	// 	for(int i = 0; i < names.length; i++) {
	// 		File GlobalChatHistoryFile = new File(Core.plugin.getDataFolder() + "/" + names[i] + ".log");
	// 		if(!GlobalChatHistoryFile.exists()) {
	// 			try (FileWriter fr = new FileWriter(Core.plugin.getDataFolder() + "/" + names[i] + ".log")) {
	// 				GlobalChatHistoryFile.createNewFile();
	// 				BufferedWriter writer = new BufferedWriter(new FileWriter(Core.plugin.getDataFolder() + "/" + names[i] + ".log"));
	// 				writer.write("");
	// 				writer.close();
	// 			} catch (IOException e) {
	// 				e.printStackTrace();
	// 			}
	// 		}
	// 	}
	// }
	
	private void RegisterEvents() {
		try {
			// _gpc.RegisterEvents(this);
			getServer().getPluginManager().registerEvents(new Events(this, 875, buffer, bubbles), this);
		} catch (Exception e) { e.printStackTrace(); }
		getLogger().info("RegisterEvents complete");
	}
	
	private void RegisterBackgroundTasks() {
		int PID1 = -1;

		if(getConfig().getBoolean("pid1")) { // Избавиться от этого уродства
			PID1 = Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			@Override
            public void run() {
				try(Connection c2 = Postgres.connect(); PreparedStatement pst2 = c2.prepareStatement("SELECT count(text) FROM hints")) {
					pst2.execute();
					try (ResultSet rs2 = pst2.getResultSet()) {
						while (rs2.next()) {
							try(PreparedStatement pst = c2.prepareStatement("SELECT text FROM hints WHERE id=" + rand.nextInt(rs2.getInt(1)))) {
								boolean isResult = pst.execute();
								do {
									try (ResultSet rs = pst.getResultSet()) {
										while (rs.next()) {
											Bukkit.broadcastMessage(ChatColor.GRAY + "[—" + rs.getString(1) + "]" + ChatColor.RESET);
										}
										isResult = pst.getMoreResults();
									}
								} while (isResult);
							}
							catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
					c2.close();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}}, 20 * 5, 20 * getConfig().getInt("pid1_time"));
		}

		if(PID1 != -1)
			this.BackgroundTasks.add(PID1);
	}

	private int secs4ticks(int secs) {
		return secs * 20;
	}

	public double maxValue(double array[]){
		return Arrays.stream(array).max().getAsDouble();
	}
	
	private void RegisterCommands() {
		try {
			// MAIN /////////////////////////////////////////////////////////
			getCommand("advert").setExecutor(new Advert());
			getCommand("info").setExecutor(new Info());
			getCommand("event").setExecutor(new Event());
			getCommand("t").setExecutor(new Tell());
			getCommand("alert").setExecutor(new Alert());
			getCommand("me").setExecutor(new Me(200));
			getCommand("try").setExecutor(new Try(100, buffer));
			getCommand("modules").setExecutor(new Modules());
//			getCommand("ignore").setExecutor(new Ignore());
			getCommand("dice").setExecutor(new Dice());

			/// SYSTEM //////////////////////////////////////////////////////
			getCommand("cc").setExecutor(new CheckChests());

			// EXPERIMENTAL /////////////////////////////////////////////////
			if(getConfig().getBoolean("ads") && getConfig().getBoolean("c2db")) {
				for(int i = 1; i < m.length; i++) {
					ItemStack is = new ItemStack(m[i]);
					t.add(is.getType().name().replace("_", " ").toLowerCase());
				}

				getCommand("ad").setExecutor(new Commands.ad());
				getCommand("ad").setTabCompleter(new Commands.ad.TC());
			}
			if(getConfig().getBoolean("police") && getConfig().getBoolean("c2db"))
				getCommand("police").setExecutor(new Police());

			/// OTHER ///////////////////////////////////////////////////////
			getCommand("pizza").setExecutor(new Pizza());
			getCommand("pokemon").setExecutor(new Pokemon());
		} catch (Exception e) { e.printStackTrace(); }
		getLogger().info("RegisterCommands complete");
	}
}
