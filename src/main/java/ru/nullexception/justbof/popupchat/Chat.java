package ru.nullexception.justbof.popupchat;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.AreaEffectCloud;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import ru.nullexception.justbof.main.Core;

public class Chat {
	private int handicapChars;
	private int readSpeed;
	private String chatFormat;

	public Chat(Core plugin) {
		this.handicapChars = plugin.getConfig().getInt("handicapChars");
		this.readSpeed = plugin.getConfig().getInt("readSpeed");
		this.chatFormat = plugin.getConfig().getString("chatFormat").replaceAll("(.)", "§$1");
	}

	int receiveMessage(Player player, String playerId, String chat) {
		String[] chatLines = chat.split("\n");
		int duration = (chat.length() + this.handicapChars * chatLines.length) * 1200 / this.readSpeed;
		Location spawnPoint = player.getLocation();
		spawnPoint.setY(-1.0D);
		Entity vehicle = player;
		for (int i = chatLines.length - 1; i >= 0; i--) {
			vehicle = spawnNameTag(vehicle, chatLines[i], spawnPoint, duration);
		}
		return duration;
	}

	private AreaEffectCloud spawnNameTag(Entity vehicle, String text, Location spawnPoint, int duration) {
		AreaEffectCloud nameTag = (AreaEffectCloud)spawnPoint.getWorld().spawnEntity(spawnPoint, EntityType.AREA_EFFECT_CLOUD);
		nameTag.setParticle(Particle.ENCHANTMENT_TABLE);
		nameTag.setRadius(0.0F);
		vehicle.addPassenger(nameTag);
		nameTag.setCustomName(String.valueOf(this.chatFormat) + text);
		nameTag.setCustomNameVisible(true);
		nameTag.setWaitTime(0);
		nameTag.setDuration(duration);
		return nameTag;
	}
}