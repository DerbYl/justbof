package ru.nullexception.justbof.popupchat;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import ru.nullexception.justbof.main.Core;

public class Buffer {
	private Core plugin;
	private int maxBubbleHeight;
	private int maxBubbleWidth;
	private int bubblesInterval;
	private Map<String, Queue<String>> chatQueue;
  
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Buffer(Core plugin) {
		this.chatQueue = new HashMap();
		this.maxBubbleHeight = plugin.getConfig().getInt("maxBubbleHeight");
		this.maxBubbleWidth = plugin.getConfig().getInt("maxBubbleWidth");
		this.bubblesInterval = plugin.getConfig().getInt("bubblesInterval");
		this.plugin = plugin;
	}
  
	public void receiveChat(Player player, String msg) {
		
		if (Character.toString(msg.charAt(0)).equals("!")) {
			return;
		}
		
		if (msg.length() <= this.maxBubbleWidth) {
			queueChat(player, String.valueOf(msg) + "\n");
			return;
		}
		
		msg = String.valueOf(msg) + " ";
		String chat = "";
		int lineCount = 0;
		
		while (msg.length() > 0) {
			int delimPos = msg.lastIndexOf(' ', this.maxBubbleWidth);
			
			if (delimPos < 0) {
				delimPos = msg.indexOf(' ', this.maxBubbleWidth);
			}
			
			if (delimPos < 0) {
				delimPos = msg.length();
			}
			
			chat = String.valueOf(chat) + msg.substring(0, delimPos);
			msg = msg.substring(delimPos + 1);
			lineCount++;
			
			if ((lineCount % this.maxBubbleHeight == 0) || (msg.length() == 0)) {
				queueChat(player, String.valueOf(chat) + (msg.length() == 0 ? "\n" : "...\n"));
				chat = "";
			} else {
				chat = String.valueOf(chat) + "\n";
			}
		}
	}
  
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void queueChat(Player player, String chat) {
		String playerId = player.getUniqueId().toString();

		if (!this.chatQueue.containsKey(playerId)) {
			this.chatQueue.put(playerId, new LinkedList());
			scheduleMessageUpdate(player, playerId, 0);
		}

		((Queue)this.chatQueue.get(playerId)).add(chat);
	}
  
	private void scheduleMessageUpdate(final Player player, final String playerId, int timer) {
		new BukkitRunnable() {
			@SuppressWarnings("rawtypes")
    		public void run() {

				if ((((Queue)Buffer.this.chatQueue.get(playerId)).size() < 1) || (!player.isOnline())) {
    				Buffer.this.chatQueue.remove(playerId);
    				return;
    			}
    			
				String chat = (String)((Queue)Buffer.this.chatQueue.get(playerId)).poll();
    			int bubbleDuration = Buffer.this.plugin.bubbles.receiveMessage(player, playerId, chat);
    			Buffer.this.scheduleMessageUpdate(player, playerId, bubbleDuration + Buffer.this.bubblesInterval);
    		}
    	}.runTaskLater(this.plugin, timer);
	}
}