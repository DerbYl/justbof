package ru.nullexception.justbof.utils;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.UUID;

import org.bukkit.Bukkit;
/**
 * Класс для работы с локальной базой данной SQLITE
 * @version EXPERIMENTAL
 * @author NullExcept1on
 */
public class Sqlite {	
	public static Connection connect(String path) {
		try {
			Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            PrintException("[SQLITE] org.sqlite.JDBC ClassNotFoundException", e);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		Connection connection = null;

		try {
			connection = DriverManager.getConnection("jdbc:sqlite:" + String.join(File.separator, path.split("/")));
			Bukkit.getLogger().info("[SQLITE] Database " + path + " has been connected");
            
            PreparedStatement pst = connection.prepareStatement("CREATE TABLE if not exists 'game_profiles' (id INTEGER PRIMARY KEY AUTOINCREMENT, access_level INTEGER, uuid VARCHAR(32))");
            pst.execute();
            PreparedStatement pst2 = connection.prepareStatement("CREATE TABLE if not exists graph1 (time INTEGER NOT NULL UNIQUE, tps INTEGER NOT NULL, players INTEGER, bb INTEGER, bp INTEGER, chat INTEGER)");
            pst2.execute();
            // PreparedStatement pst3 = connection.prepareStatement("CREATE TABLE if not exists blacklists (id INTEGER PRIMARY KEY AUTOINCREMENT, uuid VARCHAR(32) NOT NULL UNIQUE, blacklist TEXT)");
            // pst3.execute();

            pst.close();
            pst2.close();
            // pst3.close();

        } catch (Exception e) {
			PrintException("Exception", e);
		}
		{
            return connection;
        }
	}

    /**
     * [НЕБЕЗОПАСНЫЙ] Выполнение уже установленной SQL-команды
     * @param path - Путь к файлу базы даных
     * @param sql - SQL-запрос
     */
    public static void exec(String path, String sql) {
        try(Connection c = connect(path); PreparedStatement pst = c.prepareStatement(sql)) {
            // for (int i = 1; i < sqlp.length; i++) { // <T>void T[] sqlp
            //     switch(sqlp[i].getClass().getName()) {
            //         case "java.lang.String":
            //             pst.setString(i, sqlp[i].toString());
            //             break;
            //         case "java.lang.Integer":
            //             pst.setInt(i, Integer.parseInt(sqlp[i].toString()));
            //             break;
            //         default:
            //             break;
            //     }
            // }
            pst.execute();
            c.close();
        } catch (Exception e) {
            PrintException("exec method", e);
        }
    }

	public static void PrintException(String mainText) {
		PrintException(mainText, null);
	}

	public static void PrintException(String mainText, Exception e) {
        Bukkit.getLogger().info("=================[DB_EXCEPTION]==================");
        Bukkit.getLogger().info(mainText);
        if(!e.equals(null)) { e.printStackTrace(); }
        Bukkit.getLogger().info("=================================================");
	}

	
	/**
     * Изменение уровня доступа игрового профиля
	 * @param path - Путь к файлу базы даных
     * @param tableName - Таблица с игровыми профилями
     * @param uuid - uuid игрока
     * @param lvl - Новый уровень
     * @return true - без ошибок; false - с ошибками
     */
    public static boolean ChangeAL(String path, String tableName, UUID uuid, Integer lvl) {
		String sql = "UPDATE " + tableName + " SET access_level=? WHERE uuid=?"; // SQL-инъекция
        try(Connection c = connect(path); PreparedStatement pst = c.prepareStatement(sql)) {
            pst.setInt(1, lvl);
            pst.setString(2, String.join("", uuid.toString().split("-")));
            pst.execute();
            c.close();
            return true;
        } catch (Exception e) {
            PrintException("ChangeAL method", e);
            return false;
        }
	}
    
    /**
     * Сравнение уровней доступа игрового профиля
     * @param path - Путь к файлу базы даных
     * @param tableName - Таблица с игровыми профилями
     * @param lvl - Требуемый уровень
     * @param uuid - Запрашиваемый uuid игрока
     */
    public static boolean CompareAL(String path, String tableName, UUID uuid, int lvl) {
        String sql = "SELECT access_level FROM " + tableName + " WHERE uuid=?"; // SQL-инъекция
        try(Connection c = connect(path); PreparedStatement pst = c.prepareStatement(sql)) {
            pst.setString(1, String.join("", uuid.toString().split("-")));
            ResultSet rs = pst.executeQuery();
            rs.next();
            boolean a = rs.getInt(1) >= lvl;
            rs.close();
            c.close();
            System.out.println(a);
            return a;
        } catch (Exception e) {
            PrintException("CompareAL method", e);
        }
        return false;
    }

	/**
     * Удаление uuid игрока из таблицы
	 * @param path - Путь к файлу базы даных
     * @param tableName - Таблица с игровыми профилями
     * @param uuid - uuid игрока
     * @author NullExcept1on
     */
    public static void RemoveUUID(String path, String tableName, String uuid) {
        String sql = "DELETE FROM " + tableName + " WHERE uuid=?"; // SQL-инъекция
        try(Connection c = connect(path); PreparedStatement pst = c.prepareStatement(sql)) {
            pst.setString(1, uuid);
            pst.execute();
            c.close();
        } catch (Exception e) {
            PrintException("RemoveUUID method", e);
        }
	}
	
	/**
     * Добавление uuid игрока в таблицу
	 * @param path - Путь к файлу базы даных
     * @param tableName - Таблица с игровыми профилями
     * @param uuid - uuid игрока
     * @author NullExcept1on
     */
    public static void InsertUUID(String path, String tableName, UUID uuid) {
        String sql = "INSERT INTO " + tableName + " (access_level, uuid) VALUES (0, ?)"; // SQL-инъекция
        try(Connection c = connect(path); PreparedStatement pst = c.prepareStatement(sql)) {
            pst.setString(1, String.join("", uuid.toString().split("-")));
            pst.execute();
            c.close();
        } catch (Exception e) {
            PrintException("InsertUUID method", e);
        }
	}
	
	/**
     * Проверка на наличие UUID в таблице (НЕ ПРОВЕРЕНО)
	 * @param path - Путь к файлу базы даных
     * @param tableName - Таблица с игровыми профилями
     * @param uuid - uuid игрока
     * @author NullExcept1on
     */
    public static Boolean UUIDisExists(String path, String tableName, UUID uuid) {
        String sql = "SELECT 1 FROM " + tableName + " WHERE uuid=?"; // SQL-инъекция
        try(Connection c = connect(path); PreparedStatement pst = c.prepareStatement(sql)) {
            pst.setString(1, String.join("", uuid.toString().split("-")));
			ResultSet rs = pst.executeQuery();
            while (rs.next()) {
				return rs.getInt(1) == 1;
            }
            c.close();
        } catch (Exception e) {
            PrintException("UUIDisExists method", e);
        }
        return false;
    }
}



// import java.sql.Connection;
// import java.sql.DriverManager;
// import java.sql.ResultSet;
// import java.sql.SQLException;
// import java.sql.Statement;


// public class conn {
// 	public static Connection conn;
// 	public static Statement statmt;
// 	public static ResultSet resSet;
	
// 	// --------ПОДКЛЮЧЕНИЕ К БАЗЕ ДАННЫХ--------
// 	public static void Conn() throws ClassNotFoundException, SQLException 
// 	   {
// 		   conn = null;
// 		   Class.forName("org.sqlite.JDBC");
// 		   conn = DriverManager.getConnection("jdbc:sqlite:TEST1.s3db");

// 		   System.out.println("База Подключена!");
// 	   }
	
// 	// --------Создание таблицы--------
// 	public static void CreateDB() throws ClassNotFoundException, SQLException
// 	   {
// 		statmt = conn.createStatement();
// 		statmt.execute("CREATE TABLE if not exists 'users' ('id' INTEGER PRIMARY KEY AUTOINCREMENT, 'name' text, 'phone' INT);");
		
// 		System.out.println("Таблица создана или уже существует.");
// 	   }
	
// 	// --------Заполнение таблицы--------
// 	public static void WriteDB() throws SQLException
// 	{
// 		   statmt.execute("INSERT INTO 'users' ('name', 'phone') VALUES ('Petya', 125453); ");
// 		   statmt.execute("INSERT INTO 'users' ('name', 'phone') VALUES ('Vasya', 321789); ");
// 		   statmt.execute("INSERT INTO 'users' ('name', 'phone') VALUES ('Masha', 456123); ");

// 		   System.out.println("Таблица заполнена");
// 	}
	
// 	// -------- Вывод таблицы--------
// 	public static void ReadDB() throws ClassNotFoundException, SQLException
// 	   {
// 		resSet = statmt.executeQuery("SELECT * FROM users");
		
// 		while(resSet.next())
// 		{
// 			int id = resSet.getInt("id");
// 			String  name = resSet.getString("name");
// 			String  phone = resSet.getString("phone");
// 	         System.out.println( "ID = " + id );
// 	         System.out.println( "name = " + name );
// 	         System.out.println( "phone = " + phone );
// 	         System.out.println();
// 		}	
		
// 		System.out.println("Таблица выведена");
// 	    }
	
// 		// --------Закрытие--------
// 		public static void CloseDB() throws ClassNotFoundException, SQLException
// 		   {
// 			conn.close();
// 			statmt.close();
// 			resSet.close();
			
// 			System.out.println("Соединения закрыты");
// 		   }

// }