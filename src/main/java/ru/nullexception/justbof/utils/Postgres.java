package ru.nullexception.justbof.utils;

import java.sql.SQLException;
import java.util.UUID;

import ru.nullexception.justbof.main.Core;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Connection;

public class Postgres {
    public static Connection connect() {
        final String url = "jdbc:postgresql://" + Core.plugin.getConfig().getString("host") + ":" + Core.plugin.getConfig().getString("port") + "/" + Core.plugin.getConfig().getString("database");
        final String user = Core.plugin.getConfig().getString("user");
        final String pass = Core.plugin.getConfig().getString("pass");

        try {
            Class.forName("org.postgresql.Driver");
        }
        catch (ClassNotFoundException e) {
            System.out.println("org.postgresql.Driver");
            e.printStackTrace();
            return null;
        }
        
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, user, pass);
        }
        catch (SQLException e2) {
            e2.printStackTrace();
            return null;
        }
        if (connection != null) {
            return connection;
        }
        return null;
    }

    /**
     * Изменение уровня доступа игрового профиля
     * @param tableName - Таблица с игровыми профилями
     * @param uuid - uuid игрока
     * @param lvl - Новый уровень
     * @return true - без ошибок; false - с ошибками
     */
    public static boolean ChangeAL(String tableName, UUID uuid, Integer lvl) {
        String sql = "UPDATE " + tableName + " SET access_level=? WHERE uuid=?"; // SQL-инъекция
        try(Connection c = connect(); PreparedStatement pst = c.prepareStatement(sql)) {
            pst.setInt(1, lvl);
            pst.setString(2, String.join("", uuid.toString().split("-")));
            pst.execute();
            c.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Удаление uuid игрока из таблицы
     * @param tableName - Таблица с игровыми профилями
     * @param uuid - uuid игрока
     * @author NullExcept1on
     */
    public static void RemoveUUID(String tableName, String uuid) {
        String sql = "DELETE FROM " + tableName + " WHERE uuid=?"; // SQL-инъекция
        try(Connection c = connect(); PreparedStatement pst = c.prepareStatement(sql)) {
            pst.setString(1, uuid);
            pst.execute();
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Сравнение уровней доступа игрового профиля
     * @param tableName - Таблица с игровыми профилями
     * @param lvl - Требуемый уровень
     * @param uuid - Запрашиваемый uuid игрока
     */
    public static boolean CompareAL(String tableName, int lvl, String uuid) {
        String sql = "SELECT access_level FROM " + tableName + " WHERE uuid=?"; // SQL-инъекция
        try(Connection c = connect(); PreparedStatement pst = c.prepareStatement(sql)) {
            pst.setString(1, uuid);
            pst.execute();
			try (ResultSet rs = pst.getResultSet()) {
				while (rs.next()) {
					return rs.getInt(1) >= lvl;
				}
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Добавление uuid игрока в таблицу
     * @param tableName - Таблица с игровыми профилями
     * @param uuid - uuid игрока
     * @author NullExcept1on
     */
    public static void InsertUUID(String tableName, UUID uuid) {
        String sql = "INSERT INTO " + tableName + " (access_level, uuid) VALUES (0, ?)"; // SQL-инъекция
        try(Connection c = connect(); PreparedStatement pst = c.prepareStatement(sql)) {
            pst.setString(1, String.join("", uuid.toString().split("-")));
            pst.execute();
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Проверка на наличие UUID в таблице (НЕ ПРОВЕРЕНО)
     * @param tableName - Таблица с игровыми профилями
     * @param uuid - uuid игрока
     * @author NullExcept1on
     */
    public static Boolean UUIDisExists(String tableName, UUID uuid) {
        String sql = "SELECT true FROM " + tableName + " WHERE uuid=?"; // SQL-инъекция
        try(Connection c = connect(); PreparedStatement pst = c.prepareStatement(sql)) {
            pst.setString(1, String.join("", uuid.toString().split("-")));
            pst.execute();
			try (ResultSet rs = pst.getResultSet()) {
				while (rs.next()) {
					return rs.getBoolean(1);
				}
			}
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}