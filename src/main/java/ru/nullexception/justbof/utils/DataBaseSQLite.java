package ru.nullexception.justbof.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.bukkit.Bukkit;
import ru.nullexception.justbof.main.Core;

import java.io.File;
import java.sql.*;
import java.util.UUID;

public class DataBaseSQLite {
    private String _path = "";

    public DataBaseSQLite(String path) {
        this._path = path;
    }

    /**
     * [НЕБЕЗОПАСНЫЙ] Выполнение SQL-команды
     * @param sql - SQL-запрос
     * @author NullExcept1on
     */
    public void exec(String sql) {
        try(Connection c = connect(); PreparedStatement pst = c.prepareStatement(sql)) {
            pst.execute();
        } catch (Exception e) {
            PrintException("exec method", e);
        }
    }

    public Connection connect() {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            PrintException("[SQLITE] org.sqlite.JDBC ClassNotFoundException", e);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        Connection connection = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:" + String.join(File.separator, this._path.split("/")));
        } catch (Exception e) {
            PrintException("Exception", e);
        }
        {
            return connection;
        }
    }

    public boolean add2blist(String uuid) {
        String list = null;
        JsonArray arr = null;

        String sql1 = "SELECT * FROM game_profiles WHERE uuid=?";
        try(Connection c = connect(); PreparedStatement pst = c.prepareStatement(sql1)) {
            pst.setString(1, uuid);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                list = rs.getString(1);
            }
            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(list).getAsJsonObject();
            arr = mainObject.getAsJsonArray();
            for(int i = 0; i < arr.size(); i++) {
                if(arr.get(i).equals(uuid))
                    return false;
            }
            arr.add(uuid);
            Core.getBL().add(uuid);Core.getBL().remove(uuid);
            rs.close();
        } catch (Exception e) {
            PrintException("ChangeAL method", e);
        }

        String sql2 = "UPDATE game_profiles SET black_list=? WHERE";
        try(Connection c = connect(); PreparedStatement pst = c.prepareStatement(sql2)) {
            pst.setString(1, String.valueOf(arr));
            pst.execute();
        } catch (Exception e) {
            PrintException("ChangeAL method", e);
        }
        return true;
    }

    public void rmFromBlist(String uuid) {
        String list = null;
        JsonArray arr = null;

        String sql1 = "SELECT * FROM game_profiles WHERE uuid=?";
        try(Connection c = connect(); PreparedStatement pst = c.prepareStatement(sql1)) {
            pst.setString(1, uuid);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                 list = rs.getString(1);
            }
            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(list).getAsJsonObject();
            arr = mainObject.getAsJsonArray();
            for(int i = 0; i < arr.size(); i++) {
                if(arr.get(i).equals(uuid)) {
                    arr.remove(i);
                    Core.getBL().remove(uuid);
                    break;
                }
            }
            rs.close();
        } catch (Exception e) {
            PrintException("ChangeAL method", e);
        }

        String sql2 = "UPDATE game_profiles SET black_list=? WHERE";
        try(Connection c = connect(); PreparedStatement pst = c.prepareStatement(sql2)) {
            pst.setString(1, String.valueOf(arr));
            pst.execute();
        } catch (Exception e) {
            PrintException("ChangeAL method", e);
        }
    }

    /**
     * Изменение уровня доступа игрового профиля
     * @param uuid - uuid игрока
     * @param lvl - Новый уровень
     * @return true - без ошибок; false - с ошибками
     * @author NullExcept1on
     */
    public boolean ChangeAL(String uuid, Integer lvl) {
        String sql = "UPDATE game_profiles SET access_lvl=? WHERE uuid=?";
        try(Connection c = connect(); PreparedStatement pst = c.prepareStatement(sql)) {
            pst.setInt(1, lvl);
            pst.setString(2, uuid);
            pst.execute();
            c.close();
            return true;
        } catch (Exception e) {
            PrintException("ChangeAL method", e);
            return false;
        }
    }

    /**
     * Сравнение уровней доступа игрового профиля
     * @param lvl - Требуемый уровень
     * @param uuid - Запрашиваемый uuid игрока
     * @author NullExcept1on
     */
    public boolean CompareAL(String uuid, int lvl) {
        String sql = "SELECT access_lvl FROM game_profiles WHERE uuid=?";
        try(Connection c = connect(); PreparedStatement pst = c.prepareStatement(sql)) {
            pst.setString(1, uuid);
            ResultSet rs = pst.executeQuery();
            boolean a = false;
            while (rs.next()) {
                int l = rs.getInt(1);
                a = l >= lvl;
                Bukkit.getLogger().info(Integer.toString(l));
            }
            rs.close();
            c.close();
            return a;
        } catch (Exception e) {
            PrintException("CompareAL method", e);
        }
        return false;
    }

    /**
     * Удаление uuid игрока из таблицы
     * @param uuid - uuid игрока
     * @author NullExcept1on
     */
    public void RemoveUUID(String uuid) {
        String sql = "DELETE FROM game_profiles WHERE uuid=?";
        try(Connection c = connect(); PreparedStatement pst = c.prepareStatement(sql)) {
            pst.setString(1, uuid);
            pst.execute();
        } catch (Exception e) {
            PrintException("RemoveUUID method", e);
        }
    }

    /**
     * Добавление uuid игрока в таблицу
     * @param uuid - uuid игрока
     * @author NullExcept1on
     */
    public void InsertUUID(String uuid) {
        String sql = "INSERT INTO game_profiles (uuid) VALUES (?)";
        try(Connection c = connect(); PreparedStatement pst = c.prepareStatement(sql)) {
            pst.setString(1, uuid);
            pst.execute();
        } catch (Exception e) {
            PrintException("InsertUUID method", e);
        }
    }

    /**
     * Проверка на наличие UUID в таблице
     * @param uuid - uuid игрока
     * @author NullExcept1on
     */
    public Boolean UUIDisExists(String uuid) {
        String sql = "SELECT 1 FROM game_profiles WHERE uuid=?";
        try(Connection c = connect(); PreparedStatement pst = c.prepareStatement(sql)) {
            pst.setString(1, uuid);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) { // ???
                return rs.getInt(1) == 1;
            }
        } catch (Exception e) {
            PrintException("UUIDisExists method", e);
        }
        return false;
    }

    public static void PrintException(String mainText) {
        PrintException(mainText, null);
    }

    public static void PrintException(String mainText, Exception e) {
        Bukkit.getLogger().info("=================[DB_EXCEPTION]==================");
        Bukkit.getLogger().info(mainText);
        if(e != null) { e.printStackTrace(); }
        Bukkit.getLogger().info("=================================================");
    }
}