package ru.nullexception.justbof.experimental;

import org.bukkit.entity.Player;

public class BoFplayer {
    private Player _p;
    private GameProfile _gp;

    public BoFplayer(Player p) {
        this._gp = new GameProfile(p);
    }

    public Player getPlayer() {
        return this._p;
    }

    public GameProfile getGameProfile() {
        return this._gp;
    }
}