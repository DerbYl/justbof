package ru.nullexception.justbof.experimental;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import ru.nullexception.justbof.main.Core;

public class CheckChests implements CommandExecutor {
    private String logBuffer = "";

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            Player p = (Player)sender;
            if(p.isOp()) {
                if(args[0].equals("0") || args[0].equals("1") || args[0].equals("2")) {
                    System.out.println(args[0]);
                    for(Chunk c : Bukkit.getWorlds().get(Integer.parseInt(args[0])).getLoadedChunks()) {
                        for(BlockState b : c.getTileEntities()) {
                            if(b instanceof Chest){
                                Chest chest = (Chest) b;
                                logBuffer += "\n\nChest (" + chest.getX() + ";" + chest.getY() + ";" + chest.getZ() + ")";
                                int i = 1;
                                for(ItemStack is : chest.getBlockInventory().getContents()) {
                                    i++;
                                    if(is != null) {
										if(is.getType().equals(Material.RABBIT_HIDE)) {
                                            logBuffer += "\n R_H " + "(x" + is.getAmount() + ";" + i + ")";
                                        } else if(is.getType().equals(Material.EMERALD)) {
                                            logBuffer += "\n E " + "(x" + is.getAmount() + ";" + i + ")";
                                        } else if(is.getType().equals(Material.EMERALD_BLOCK)) {
                                            logBuffer += "\n E_B " + "(x" + is.getAmount() + ";" + i + ")";
                                        } else if(is.getType().equals(Material.BEACON)) {
                                            logBuffer += "\n B " + "(x" + is.getAmount() + ";" + i + ")";
                                        }
                                    }
                                }
                                try {
                                    Files.write(Paths.get(Core.plugin.getDataFolder() + "/cc.log"), logBuffer.getBytes(), StandardOpenOption.APPEND);
                                    logBuffer = "";
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }
    
}