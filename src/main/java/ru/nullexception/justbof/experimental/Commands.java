package ru.nullexception.justbof.experimental;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import ru.nullexception.justbof.main.Core;

public class Commands {
    public static class ad implements CommandExecutor {
        /**
         * Расширенная версия команды /advert
         */
        @Override
        public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
            if(sender instanceof Player) {
                // Player p = (Player)sender;
                if(args.length > 0) {
                    // TextComponent msg = new TextComponent(ChatColor.YELLOW + "[" + args[2].toUpperCase() + "] " + StringHelper.splitWords(3, args, true) + " (" + sender.getName() + ")");
                    // msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Цена сделки: " + args[0] + " (" + args[1] + ")").create()));
                    // msg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "adr "));
                    // p.spigot().sendMessage(msg);
                    // try (Connection conn = Postgres.connect(); PreparedStatement pst = conn.prepareStatement("INSERT ")) {
                    //     pst.execute();
                    // } catch (Exception e) {
                    //     e.printStackTrace();
                    //     p.sendMessage(ChatColor.RED + "Проблемы с регистрацией лота. Обратитесь к разработчику." + ChatColor.RESET);  // Вставить текст stacktrace в компонент
                    // }

                    // Material[] m = Material.values();
                    // for(int i = 0; i < m.length && i < 5; i++){
                    //     ItemStack is = new ItemStack(m[i]);
                    //     Bukkit.broadcastMessage(is.getType().name());
                    // }
                    return true;
                }
            }
            return false;
        }

        public static class TC implements TabCompleter {
            private static final List<String> Finance = Arrays.asList("шкурки", "порох", "другое");
            // private static final List<String> Action = Arrays.asList("куплю", "продам", "реклама", "найм", "обмен");

            @Override
            public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
                switch(args.length) {
                    case 1:
                        return StringUtil.copyPartialMatches(args[args.length - 1], Finance, new ArrayList<>());
                    case 3:
                        return StringUtil.copyPartialMatches(args[args.length - 1], Core.t, new ArrayList<>());
                }
                return null;
            }
        }
    }
}