package ru.nullexception.justbof.experimental.pc;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import ru.nullexception.justbof.experimental.BoFplayer;
import ru.nullexception.justbof.main.Core;

public class PlayerController {
    private ArrayList<BoFplayer> _table = new ArrayList<BoFplayer>();

    public void registerPlayer(Player p) {
        this._table.add(new BoFplayer(p));
    }
    
    public void RegisterEvents(Core core) {
        Bukkit.getLogger().info("[PlayerController > Constructor] register PlayerControllerListener");
        Core.plugin.getServer().getPluginManager().registerEvents(new PlayerControllerListener(), Core.plugin);
    }

    public void RegisterSchedules() {
        Core.plugin.getServer().getScheduler().scheduleSyncRepeatingTask(Core.plugin, new Runnable() {
            @Override
            public void run() {
                if(_table.size() > 0) {
                    _table.forEach(gp -> {
                        gp.getGameProfile().addGT();
                    });
                }
            }
        }, 20, 20);

        Core.plugin.getServer().getScheduler().scheduleSyncRepeatingTask(Core.plugin, new Runnable() {
            @Override
            public void run() {
                if(_table.size() > 0) {
                    _table.forEach(gp -> gp.getGameProfile().update2db());
                } else {
                    Core.plugin.getLogger().info("[PlayerController > Schedule #2] Update GameProfiles was skipped");
                }
            }
        }, 20 * 10, 20 * 60);
    }

    public void unregisterPlayer(Player p) {
        this.unregisterPlayer(p, null);
    }

    public void unregisterPlayer(Player p, String reason) {
        try {
            this._table.forEach(bofplayer -> {
                if(bofplayer.getGameProfile().getUUID().equals(String.join("", p.getUniqueId().toString().split("-")))) {
                    this._table.remove(bofplayer);
                    if(p.isOnline() && reason != null) {
                        p.kickPlayer("Вы были отключены контроллером JustBoF по причине: " + reason);
                    }
                    return;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<BoFplayer> getGameProfiles() {
        return this._table;
    }
}