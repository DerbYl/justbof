package ru.nullexception.justbof.experimental;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.UUID;

import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import ru.nullexception.justbof.main.Core;
import ru.nullexception.justbof.utils.Postgres;
import ru.nullexception.justbof.utils.Sqlite;

public class GameProfile {
    private String _uuid = null;
    private int _blocks_broken = 0;
    private int _blocks_placed = 0;
    private int _game_time = 0;
    // private HashMap<UUID, Integer> _damage_vs_players;
    // private ArrayList<UUID> _uuid_history;
    
    public GameProfile(Player p) {
        UUID uuid = p.getUniqueId();
        this._uuid = String.join("", uuid.toString().split("-"));
        switch(Core.plugin.getConfig().getInt("game_profile_database_type")) {
            case 1:
                if(!Sqlite.UUIDisExists(Core.plugin.getConfig().getString("SQLite_path"), Core.plugin.getConfig().getString("game_profiles_table"), p.getUniqueId())) {
                    Sqlite.InsertUUID(Core.plugin.getConfig().getString("SQLite_path"), Core.plugin.getConfig().getString("game_profiles_table"), p.getUniqueId());
                    p.sendMessage(ChatColor.GRAY + "На Вас был заведён игровой профиль " + ChatColor.STRIKETHROUGH + "для доноса" + ChatColor.GRAY + " приятной игры");
                }
                break;
            case 2:
                if(!Postgres.UUIDisExists(Core.plugin.getConfig().getString("game_profiles_table"), p.getUniqueId())) {
                    Postgres.InsertUUID(Core.plugin.getConfig().getString("game_profiles_table"), p.getUniqueId());
                    p.sendMessage(ChatColor.GRAY + "На Вас был заведён игровой профиль " + ChatColor.STRIKETHROUGH + "для доноса" + ChatColor.GRAY + " приятной игры");
                }
                break;
            default:
                return;
        }

        // if(Sqlite.UUIDisExists(Core.plugin.getConfig().getString("SQLite_path"), Core.plugin.getConfig().getString("game_profiles_table"), uuid)) {
        try(Connection c = Sqlite.connect(Core.plugin.getConfig().getString("SQLite_path")); PreparedStatement pst = c.prepareStatement("SELECT * FROM "+Core.plugin.getConfig().getString("game_profiles_table")+" WHERE uuid=?")) {
            pst.setString(1, this._uuid);
            pst.execute();
            // do {
                try (ResultSet rs = pst.getResultSet()) {
                    while (rs.next()) {
                        this._blocks_broken = rs.getInt("blocks_broken");
                        this._blocks_placed = rs.getInt("blocks_placed");
                        this._game_time = rs.getInt("game_time");
                        Core.plugin.getLogger().info("[GameProfile > Constructor] " + uuid.toString() + " - "+Core.plugin.getServer().getPlayer(uuid).getName()+" has been loaded");
                    }
                    // isResult = pst.getMoreResults();
                }
            // } while (isResult);
            c.close();
        } catch (Exception e) {
            Sqlite.PrintException("GameProfile", e);
        }
        // } else {
        //     this._blocks_broken = 0;
        //     this._blocks_placed = 0;
        //     // this._damage_vs_players = new HashMap<UUID, Integer>();
        //     this._game_time = 0;
        //     // this._uuid_history = new ArrayList<UUID>();
        //     try(Connection c = Sqlite.connect(Core.plugin.getConfig().getString("SQLite_path")); PreparedStatement pst = c.prepareStatement("INSERT INTO "+Core.plugin.getConfig().getString("game_profiles_table")+" (current_uuid) VALUES (?)")) {
        //         pst.setString(1, String.join("", uuid.toString().split("-")));
        //         pst.execute();
        //         c.close();
        //     } catch (Exception e) {
        //         Sqlite.PrintException("GameProfile", e);
        //     }
        // }
    }

    // GameProfile(int bb, int bp, int gt, HashMap<UUID, Integer> dvp, ArrayList<UUID> uh) {
    //     this._blocks_breaked = bb;
    //     this._blocks_placed = bp;
    //     this._game_time = gt;
    //     this._damage_vs_players = dvp;
    //     this._uuid_history = uh;
	// }

    public String getUUID() {
        return this._uuid;
    }

    public void addBB() {
        this._blocks_broken += 1;
    }

    public int getBB() {
        return this._blocks_broken;
    }

    public void addBP() {
        this._blocks_placed += 1;
    }

    public int getBP() {
        return this._blocks_placed;
    }

    public void addGT() {
        this._game_time += 1;
    }

    public int getGT() {
        return this._game_time;
    }

    public void update2db() {
        try(Connection c = Sqlite.connect(Core.plugin.getConfig().getString("SQLite_path")); PreparedStatement pst = c.prepareStatement("INSERT INTO "+Core.plugin.getConfig().getString("game_profiles_table")+" (blocks_broken, blocks_placed, game_time) VALUES (?, ?, ?) WHERE uuid=?")) {
            pst.setInt(1, this.getBB());
            pst.setInt(2, this.getBP());
            pst.setInt(3, this.getGT());
            pst.setString(4, this._uuid);
            pst.execute();
            c.close();
        } catch (Exception e) {
            Sqlite.PrintException("GameProfile", e);
        }
    }

    // public void addDVP(UUID uuid, int n) {
    //     this._damage_vs_players.put(uuid, this._damage_vs_players.get(uuid) + n);
    // }

    // public HashMap<UUID, Integer> getDVP() {
    //     return this._damage_vs_players;
    // }

    // public void addUH(UUID uuid) {
    //     this._uuid_history.add(uuid);

    // }

    // public ArrayList<UUID> getUH() {
    //     return this._uuid_history;
    // }
}