package ru.nullexception.justbof.cmds;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ru.nullexception.justbof.lib.ConsoleColor;
import ru.nullexception.justbof.lib.StringHelper;

public class Advert implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		String prefix;
		if(args.length > 0 && sender instanceof Player) {
			switch(args[0].toLowerCase()) {
				case "куплю":
					prefix = "[КУПЛЮ]";
					break;
				case "продам":
					prefix = "[ПРОДАМ]";
					break;
				case "реклама":
					prefix = "[РЕКЛАМА]";
					break;
				default:
					return false;
			}
			
			Bukkit.broadcastMessage(ChatColor.YELLOW + prefix  + " " + StringHelper.splitWords(1, args, true) + " (" + sender.getName() + ")");
			return true;
		} else {
			if(!(sender instanceof Player)) {
				Bukkit.getLogger().info(ConsoleColor.RED + "/advert only player!" + ConsoleColor.RESET);
			}
		}
		return false;
	}
}