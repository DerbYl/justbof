package ru.nullexception.justbof.cmds;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.nullexception.justbof.main.Core;

public class Ignore implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
        if(args.length > 0 && sender instanceof Player) {
            Player p = (Player) sender;
            switch (args[0]) {
                case "add":
                    for(Player pl : Bukkit.getOnlinePlayers()) {
                        if(args[1].equals(pl.getName())) {
                            if(Core.getDataBaseGameData().add2blist(String.join("", pl.getUniqueId().toString().split("-")))) {
                                p.sendMessage(ChatColor.RED + "Игрок добавлен в чёрный список");
                            } else {
                                p.sendMessage(ChatColor.RED + "Игрок уже в чёрном списке");
                            }
                            return true;
                        }
                    }
                case "rm":
                    for(Player pl : Bukkit.getOnlinePlayers()) {
                        if(args[1].equals(pl.getName())) {
                            Core.getDataBaseGameData().rmFromBlist(String.join("", pl.getUniqueId().toString().split("-")));
                            p.sendMessage(ChatColor.RED + "Игрок удалён из чёрного списка");
                            return true;
                        }
                    }
            }
        }
        return false;
    }
}