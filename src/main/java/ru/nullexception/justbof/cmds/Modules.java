package ru.nullexception.justbof.cmds;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import ru.nullexception.justbof.main.Core;

public class Modules implements CommandExecutor {
    private String check(boolean flag) {
        return "[" + (flag ? ChatColor.GREEN + "ON" : ChatColor.RED + "OFF") + ChatColor.RESET + "]";
    }
    
    private String check(String name) {
        return "[" + (Core.plugin.getConfig().getBoolean(name) ? ChatColor.GREEN + "ON" : ChatColor.RED + "OFF") + ChatColor.RESET + "]";
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            Player p = (Player) sender;
            
            boolean cmd_police = (Core.plugin.getConfig().getBoolean("c2db") && Core.plugin.getConfig().getBoolean("police"));
            boolean cmd_ad = (Core.plugin.getConfig().getBoolean("c2db") && Core.plugin.getConfig().getBoolean("ad"));

            p.sendMessage(
                "Модули плагина:\n" +
                "База данных " + check("c2db") + "\n" +
                "\n" +
                "Команды плагина:\n" +
                "/police: " + check(cmd_police) + "\n" + 
                "/ad: " + check(cmd_ad)
            );
        }
        return true;
    }
}