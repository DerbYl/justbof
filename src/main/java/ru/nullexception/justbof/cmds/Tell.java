package ru.nullexception.justbof.cmds;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ru.nullexception.justbof.lib.StringHelper;
import ru.nullexception.justbof.main.Core;
import ru.nullexception.justbof.lib.ServerActions;

public class Tell implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			if(args.length > 0) {
				if(Bukkit.getPlayer(args[0]) == null) { sender.sendMessage(ChatColor.RED + "Игрок " + args[0] + " не найден"); return true; }
				Bukkit.getLogger().info(
					"p, " + player.getName() + ", " + StringHelper.splitWords(1, args, true) + " [" +
					Double.toString(player.getLocation().getX()) + ";" +
					Double.toString(player.getLocation().getY()) + ";" +
					Double.toString(player.getLocation().getZ()) +
				"]");
				return ServerActions.privateMessage(player, Bukkit.getPlayer(args[0]), args.length > 1 ? StringHelper.splitWords(1, args, true) : "(Пустое сообщение)");
			}
			return false;
		} else {
			if(args.length > 0) {
				if(Bukkit.getPlayer(args[0]) == null) { sender.sendMessage(ChatColor.RED + "Игрок " + args[0] + " не найден"); return true; }
				return ServerActions.privateMessageFromCMD(Bukkit.getPlayer(args[0]), args.length > 1 ? StringHelper.splitWords(1, args, true) : "(Пустое сообщение)");
			}
			return false;
		}
	}
}