package ru.nullexception.justbof.cmds;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class Info implements CommandExecutor {
    @Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
        sender.sendMessage("Плагин JustBoF разработан исключительно для серверов сообщества BoF. Идея плагина заключается в решении простых запросов. Так же его код находится в OpenSource (https://gitlab.com/bof-engineeringtteam/justbof). Вы можете внести свой вклад в развитие этого плагина или взять наш код для разработки своего игрового проекта.");
        return true;
    }
}