package ru.nullexception.justbof.cmds;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import ru.nullexception.justbof.main.Core;

/**
 * Команда для наделения игрока правами полицейского
 * 
 * @version EXPERIMENTAL
 * @author NullExcept1on
 */
public class Police implements CommandExecutor {
    @Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
        if(args.length > 0 && sender instanceof Player) {
            Player player = (Player)sender;
            if(!player.isOp()) {
                if(!Core.getDataBaseGameData().CompareAL(String.join("", player.getUniqueId().toString().split("-")), 1)) {
                    player.sendMessage(ChatColor.RED + "Нет прав");
                    return true;
                }
            }
            switch(args[0]) {
                case "add":
                    for(Player p : Bukkit.getOnlinePlayers()) {
                        if(args[1].equals(p.getName())) {
                            boolean result = Core.getDataBaseGameData().ChangeAL(String.join("", p.getUniqueId().toString().split("-")), 1);
                            if(result) {
                                player.sendMessage("На игрока " + p.getName() + " (" + p.getUniqueId().toString() + ") оформлен статус полицейского");
                                p.sendMessage(ChatColor.GREEN + "Игрок " + player.getName() + " присвоил Вам права полицейского");
                            } else {
                                player.sendMessage(ChatColor.RED + "Проблема с оформлением статуса полицейского. Обратитесь к разработчику (" + Core.plugin.getConfig().getString("FeedBackURL") + ")");  // Вставить текст stacktrace в компонент
                            }
                            return true;
                        }
                    }
                    player.sendMessage(ChatColor.RED + "Игрок " + args[1] + " не найден на сервере");
                    return true;
                case "rm":
                    for(Player p : Bukkit.getOnlinePlayers()) {
                        if(args[1].equals(p.getName())) {
                            if(!Core.getDataBaseGameData().UUIDisExists(String.join("", p.getUniqueId().toString().split("-")))) {
                                player.sendMessage("Игровой профиль (" + p.getName() + ") не найден в базе данных");
                                p.sendMessage(ChatColor.RED + "Ваш профиль не найден в базе данных. Перезайдите для решения этой проблемы");
                            } else {
                                boolean result = Core.getDataBaseGameData().ChangeAL(String.join("", p.getUniqueId().toString().split("-")), 0);
                                if(result) {
                                    player.sendMessage("С игрока " + p.getName() + " (" + p.getUniqueId().toString() + ") снят статус полицейского");
                                    p.sendMessage(ChatColor.RED + "Игрок " + player.getName() + " снял с Вас права полицейского");
                                }
                                else {
                                    player.sendMessage(ChatColor.RED + "Проблема с оформлением статуса полицейского. Обратитесь к разработчику (" + Core.plugin.getConfig().getString("FeedBackURL") + ")");  // Вставить текст stacktrace в компонент
                                }
                            }
                            return true;
                        }
                    }
                    player.sendMessage(ChatColor.RED + "Игрок " + args[1] + " не найден на сервере");
                    return true;
                default:
                    return false;
            }
        }
        return true;
    }
}