package ru.nullexception.justbof.cmds;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ru.nullexception.justbof.lib.StringHelper;

public class Me implements CommandExecutor {
	int maxDistance;

	public Me(int Distance) {
		this.maxDistance = Distance;
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length != 0) {
			Player player = (Player) sender;
			String message = StringHelper.splitWords(args, true);

			for (Player p : player.getWorld().getPlayers()) {
				if (p.getLocation().distanceSquared(player.getLocation()) <= this.maxDistance) {
					p.sendMessage(ChatColor.LIGHT_PURPLE + player.getName() + " " + message);
				}
			}
			return true;
		}
		return false;
	}
}