package ru.nullexception.justbof.cmds;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import ru.nullexception.justbof.lib.StringHelper;
import ru.nullexception.justbof.popupchat.Buffer;

public class Try implements CommandExecutor {
	int maxDistance;
	private Buffer buffer;

	public Try(int Distance, Buffer buffer) {
		this.maxDistance = Distance;
		this.buffer = buffer;
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Random random = new Random();
		String result = (random.nextBoolean() ? ChatColor.GREEN + "удачно": ChatColor.RED + "неудачно") + ChatColor.RESET;
		
		if(sender instanceof ConsoleCommandSender) {
			Bukkit.getLogger().info("Только для игроков");
			return true;
		}
		
		String randResChat = ChatColor.LIGHT_PURPLE + sender.getName() + ChatColor.WHITE + StringHelper.splitWords(args, false) + " | " + result;
		String randResChatB = "[" + result + "]";
		
		if(sender instanceof Player && args.length >= 1) {
			Player player = (Player) sender;
			for (Player p : player.getWorld().getPlayers()) {
				if (p.getLocation().distanceSquared(player.getLocation()) <= this.maxDistance) {
					p.sendMessage(randResChat);
				}
			}
			buffer.receiveChat(player, randResChatB);
			return true;
		}
		return false;
	}
}