package ru.nullexception.justbof.cmds;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ru.nullexception.justbof.lib.StringHelper;

public class Event implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(args.length > 0) {
			if(sender instanceof Player) {
				Player player = (Player) sender;
				Location pLoc = player.getLocation();
				
				TextComponent author = new TextComponent(ChatColor.GOLD + "@" + sender.getName() + ChatColor.RESET);
				author.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/t " + sender.getName() + " "));
				author.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Нажмите для отправки ЛС").create()));
				
				TextComponent position = new TextComponent(ChatColor.GREEN + "XYZ: " + Integer.toString(pLoc.getBlockX()) + "; " + Integer.toString(pLoc.getBlockY()) + "; " + Integer.toString(pLoc.getBlockZ()));
				
				TextComponent msg = new TextComponent(
					ChatColor.GREEN + "▰▰▰▰ИВЕНТ▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰\n" +
					ChatColor.RESET + StringHelper.splitWords(args, true)
				);
				
				msg.addExtra("\n");
				msg.addExtra(author);
				msg.addExtra(" ");
				msg.addExtra(position);
				msg.addExtra("\n");
				msg.addExtra(ChatColor.GREEN + "▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰" + ChatColor.RESET);
				
				Bukkit.getServer().spigot().broadcast(msg);

			} else {
				TextComponent msg = new TextComponent(
					ChatColor.GREEN + "▰▰▰▰ИВЕНТ▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰\n" + ChatColor.RESET +
					StringHelper.splitWords(args, true) +
					"\n" + ChatColor.GOLD + "@Админ"
				);
				msg.addExtra(ChatColor.GREEN + "▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰");
				Bukkit.getServer().spigot().broadcast(msg);
			}
			return true;
		}
		return false;
	}
}