package ru.nullexception.justbof.cmds;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import ru.nullexception.justbof.lib.StringHelper;
import ru.nullexception.justbof.utils.Postgres;

public class Alert implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			if(args.length > 0) {
				if(player.isOp() || Postgres.CompareAL("server1_game_profile", 2, String.join("", player.getUniqueId().toString().split("-")))) {
					Bukkit.broadcastMessage(ChatColor.RED + "" + ChatColor.BOLD
					+ "▰▰▰▰ОБЪЯВЛЕНИЕ▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰\n" + ChatColor.RESET + ChatColor.RED + "[" + ChatColor.RESET 
					+ StringHelper.splitWords(args, true) + ChatColor.RED
					+ "]\n▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰");
				} else {
					player.sendMessage(ChatColor.RED + "Нет прав");
				}				
				return true;
			}
		} else if(sender instanceof ConsoleCommandSender) {
			Bukkit.broadcastMessage(ChatColor.RED + "" + ChatColor.BOLD
					+ "▰▰▰▰ОБЪЯВЛЕНИЕ▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰\n" + ChatColor.RESET + ChatColor.RED + "[" + ChatColor.RESET 
					+ StringHelper.splitWords(args, true) + ChatColor.RED
					+ "]\n▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰");
			return true;
		}
		return false;
	}
}