package ru.nullexception.justbof.lib;

public class StringHelper {
	public static String firstUpperCase(String word) {
		if ((word == null) || (word.isEmpty())) {
			return "";
		}
		return word.substring(0, 1).toUpperCase() + word.substring(1);
	}
	
	/**
	 * <h1>Объединение введённых слов в предложение после команды</h1>
	 */
	public static String splitWords(String[] args, boolean upper) {
		String message = "";
		int currentIndex = 0;
		if (args.length >= 1) {
			if(upper) { message = firstUpperCase(args[0]); currentIndex = 1;}
			for (int i = currentIndex; i < args.length; i++) {
				message += " " + args[i];
			}
			return message;
		}
		return "<null>";
	}
	
	/**
	 * <h1>Объединение введённых слов в предложение после слова по индексу</h1>
	 * Пример: <i>splitWords(1, "/advert куплю что-нибудь") >> "что-нибудь"</i>
	 */
	public static String splitWords(int index, String[] args, boolean upper) {
		String message = "";
		int currentIndex = index;
		if (args.length > 1) {
			if(upper) { message = firstUpperCase(args[index]); currentIndex += 1;}
			for (int i = currentIndex; i < args.length; i++) {
				message += " " + args[i];
			}
			return message;
		}
		return "<null>";
	}
}