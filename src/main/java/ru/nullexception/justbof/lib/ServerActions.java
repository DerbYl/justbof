package ru.nullexception.justbof.lib;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class ServerActions {
	public static String welcomeMessage(Player player) {
		return ChatColor.GREEN + "Добро пожаловать домой, " + player.getName() + "!";
    }
	
	public static void localMessage(Player player, String message, int maxDistance) {
		for (Player p : player.getWorld().getPlayers()) {
			if (p.getLocation().distanceSquared(player.getLocation()) <= maxDistance) {
				TextComponent msg = new TextComponent(net.md_5.bungee.api.ChatColor.LIGHT_PURPLE + "(" + player.getName() + ") " + net.md_5.bungee.api.ChatColor.RESET + message);
				msg.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/t " + player.getName() + " "));
				msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Нажмите для отправки ЛС").create()));
				p.spigot().sendMessage(msg);
			}
		}
	}
	
	public static boolean privateMessage(Player playerFrom, Player playerTo, String message) {
		if(playerTo.isOnline()) {
			playerFrom.sendMessage("@" + ChatColor.GREEN + playerTo.getName() + ChatColor.WHITE +  " " + ChatColor.GOLD + message);
			
			TextComponent msg = new TextComponent(
				net.md_5.bungee.api.ChatColor.RESET + "[" + net.md_5.bungee.api.ChatColor.GREEN + playerFrom.getName() + net.md_5.bungee.api.ChatColor.WHITE + "] "
			);
			msg.addExtra(message);
			msg.setColor(net.md_5.bungee.api.ChatColor.GOLD);
			msg.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/t " + playerFrom.getName() + " "));
			msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Нажмите для отправки ЛС").create()));
			playerTo.spigot().sendMessage(msg);
			return true;
		}
		return false;
	}
	
	public static boolean privateMessageFromCMD(Player playerTo, String message) {
		if(playerTo.isOnline()) {
			playerTo.sendMessage("[" + ChatColor.GREEN + "Админ" + ChatColor.WHITE + "] " + ChatColor.GOLD + message);
			playerTo.playSound(playerTo.getLocation(), Sound.ENTITY_CAT_PURREOW, 50, 50);
			return true;
		}
		return false;
	}
}
