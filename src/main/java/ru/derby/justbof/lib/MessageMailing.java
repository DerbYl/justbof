package lib;

import org.bukkit.entity.Player;

public class MessageMailing {
    public static void localMessage(Player player, String message, int distance){
        for (Player p : player.getWorld().getPlayers()) {
            if (p.getLocation().distanceSquared(player.getLocation()) <= distance) {
                p.sendMessage(message);
            }
        }
    }
}