package cmds;

import ru.derby.justbof.lib.MessageMailing;
import ru.nullexception.justbof.lib.StringHelper;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class Do implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            String message = ChatColor.LIGHT_PURPLE + StringHelper.splitWords(args, true) + " (( " + player.getDisplayName() + " ))";

            MessageMailing.localMessage(player, message, 50);
            return true;
        }
        return false;
    }
}
