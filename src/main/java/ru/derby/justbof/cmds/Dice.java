package ru.derby.justbof.cmds;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import java.util.Random;


public class Dice implements Listener, CommandExecutor {


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] arg) {

        int dice = -999;
        String mess = "";

        if (sender instanceof Player){
            Player player = (Player) sender;
            try {

                if (arg.length == 1) { // arg[0] - maxNum

                    dice = getRandomNumberInRange(1, Integer.valueOf(arg[0]));
                    mess = "(1:"+ arg[0] + ")";

                } else if (arg.length == 2) { // arg[0] - minNum, arg[1]- maxNum

                    dice = getRandomNumberInRange(Integer.valueOf(arg[0]), Integer.valueOf(arg[1]));
                    mess = "("+ arg[0] + ":" + arg[1] + ")";
                } else{ // Если arg,length > 2 or == 0
                    dice = getRandomNumberInRange(1, 6);
                    mess = "(1:6)";
                }
            }catch (NumberFormatException ex) { // Сработает, если введено не число
                player.sendMessage(ChatColor.RED + "Введи целое число!");
                return false;
            }

            
            for (Player p : player.getWorld().getPlayers()) {
				if (p.getLocation().distanceSquared(player.getLocation()) <= 50) {
					p.sendMessage(ChatColor.GOLD + player.getDisplayName() + " " + mess + ", его чило " + dice);
				}
			}
            
            return true;
        }
        return false;
    }

    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            return 0;
        }
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
}
