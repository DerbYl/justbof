from mcrcon import MCRcon
from termcolor import colored
import click, subprocess, os, time

ip_game_server = ''
rcon_password = ''
plugin_name = ''
destination = ''
f_scp = False
comment = ''
version = 0
direct = 0

@click.command()
@click.option('--pn', help='Название файла плагина')
@click.option('--dst', help='Где должен оказаться плагин?')
@click.option('--scp/--no-scp', default=False, help='Использовать scp?')
@click.option('--ip', default='127.0.0.1', help='IP сервера')
@click.option('--rconpass', help='rcon пароль')
def prepare(pn, dst, scp, ip, rconpass):
    global plugin_name, f_scp, ip_game_server, rcon_password, direct, comment, version
    direct = input('Направление (0 - patch, 1 - release, 2 - hotfix, 3 - shapshot) (3): ')
    if direct == '':
        direct = '3'
    comment = input('Комментарий сборки (dev): ')
    if comment == '':
        comment = 'dev'
    version = input('Версия сборки (0): ')
    if version == '':
        version = '0'
    plugin_name = pn
    destination = dst
    f_scp = scp
    ip_game_server = ip
    rcon_password = rconpass
    rc1 = _runProcess('mvn clean install')
    if rc1 == 0:
        if not f_scp:
            rconCmd("127.0.0.1", "1234", "text [§cBoF§f] Сборка плагина " + plugin_name + " §aуспешна§f")
            _runProcess('mv target/' + plugin_name + '.jar {0}'.format(destination).replace('/', os.sep))
            rconCmd("127.0.0.1", "1234", "reload confirm")
            rconCmd("127.0.0.1", "1234", 'buildinfo {0} {1} {2} "{3}"'.format(direct, version, plugin_name, comment))
        else:
            _runProcess('scp target' + os.sep + plugin_name + '.jar nullexcept1on@mc.playbof.ru:~/BoFseason2/plugins/')
            time.sleep(3)
            rconCmd(ip_game_server, rcon_password, "reload confirm")
            rconCmd(ip_game_server, rcon_password, 'buildinfo {0} {1} {2} "{3}"'.format(direct, version, plugin_name, comment))
    else:
        if not f_scp:
            rconCmd("127.0.0.1", "1234", "text [§cBoF§f] Сборка плагина " + plugin_name + " §cпроблемная§f")
        else:
            rconCmd(ip_game_server, rcon_password, "text [§cBoF§f] Сборка плагина " + plugin_name + " §cпроблемная§f")

def _runProcess(cmd):
    process = subprocess.Popen(cmd.split(' '), stdout=subprocess.PIPE, universal_newlines=True)
    while True:
        output = process.stdout.readline()
        print(output.strip())
        return_code = process.poll()
        if return_code is not None:
            return return_code
    process.stdout.close()

def rconCmd(ip: str, passwd: str, cmd: str):
    try:
        mcr = MCRcon(ip, passwd)
        mcr.connect()
        resp = mcr.command(cmd)
        mcr.disconnect()
        return resp
    except ConnectionRefusedError:
        colored('[MCRCON] Connection refused', 'red')

if __name__ == "__main__":
    prepare()